# Change Log

## [UNRELEASED]

## [0.2.0] - 2018-08-16
### Added
- Python3 support, python2 backwards-compat

## [0.1.6] - 2017-10-20
### Fixed
- revert unique constraint piece

## [0.1.5] - 2017-10-20
### Fixed
- create migration structure

## [0.1.4] - 2017-10-20
### Fixed
- Change package name in setup.py to eav-django-10B
- Bump version number

## [0.1.3] - 2017-04-26
### Fixed
- Change package name in setup.py to eav-django
- Bump version number

## [0.1.2] - 2017-03-30
### Added
- added unique=True to title field of BaseChoice Model

## [0.1.1] - 2017-03-10
### Added
- attrs references on base Schema changed to attributes
- allows baseentitity manager to be used.

## [0.2.0] - 2017-03-01
### Added
- Validator that checks value against reserved field names _(e.g., name, relations)_
- Add validator to `Schema.title` field

## [0.1.0] - 2016-11-21
### Added
- This CHANGELOG.md file!

### Fixed
- Pin setup.py version to 0.1.0
- Refactor project for upgrade to Django-1.9

