"""
Validators
~~~~~~~~~~
"""

from django.conf import settings
from django.core.exceptions import ValidationError

RESERVED_ATTRIBUTE_NAMES = []
if settings.RESERVED_ATTRIBUTE_NAMES:
    RESERVED_ATTRIBUTE_NAMES = settings.RESERVED_ATTRIBUTE_NAMES


def validate_reserved_field_name(value):
    """
    Validate that the name of an attribute schema does not conflict with field
    names on the listings model.
    """
    if value.lower() in RESERVED_ATTRIBUTE_NAMES:
        msg = 'Schema name "%s" is a reserved field name, please choose another'
        raise ValidationError(msg % value)
